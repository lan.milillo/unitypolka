using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideHud : MonoBehaviour
{
    public GameObject Canvas;
    bool active=true;
 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SetActiveBox(Canvas);
        
        
    }

    public void SetActiveBox(GameObject Canvas)
    {
        if (active)
        {
            if (Input.GetKeyDown(KeyCode.H))
            {
                Canvas.SetActive(false);
                active = false;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.H))
            {
                Canvas.SetActive(true);
                active = true;
            }
        }
    }

 
}
