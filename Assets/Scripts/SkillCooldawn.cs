using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillCooldawn : MonoBehaviour
{
    public KeyCode Button;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UseSkill();
    }

    public void UseSkill()
    {
        if (Input.GetKeyDown(Button))
        {
            gameObject.GetComponent<Image>().color = new Color32(0, 0, 0, 150);
            gameObject.GetComponent<FillAmount>().setCoolingDown();

        }
    }
}
