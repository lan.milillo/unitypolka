using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideShow : MonoBehaviour
{
    public GameObject Canvas;
    bool active = false;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
  

    public void SetActiveBox()
    {
        if (active)
        {
                Canvas.SetActive(false);
                active = false;   
        }
        else
        {
                Canvas.SetActive(true);
                active = true;   
        }
    }


}
