using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FillAmount : MonoBehaviour
{
    // Start is called before the first frame update
    public Image cooldown;
    public bool coolingDown;
    public float waitTime = 30.0f;

    // Update is called once per frame
    void Update()
    {
        if (coolingDown == true)
        {
            //Reduce fill amount over 30 seconds
            cooldown.fillAmount -= 1.0f / waitTime * Time.deltaTime;
        }

        if (cooldown.fillAmount == 0)
        {
            coolingDown = false;
            cooldown.fillAmount = 1;
            gameObject.GetComponent<Image>().color = new Color32(0, 0, 0, 0);
        }
        
    }

    public void setCoolingDown()
    {
        coolingDown = coolingDown = true;
    }
    
}

